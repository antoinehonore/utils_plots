import numpy as np
import matplotlib.pyplot as plt

# Plotly colors
ALL_COLORS = ["aliceblue", "antiquewhite", "aqua", "aquamarine", "azure",
                "beige", "bisque", "black", "blanchedalmond",
                "blueviolet", "brown", "burlywood", "cadetblue",
                "chartreuse", "chocolate", "coral", "cornflowerblue",
                "cornsilk", "crimson", "cyan", "darkblue", "darkcyan",
                "darkgoldenrod", "darkgray", "darkgrey", "darkgreen",
                "darkkhaki", "darkmagenta", "darkolivegreen", "darkorange",
                "darkorchid", "darkred", "darksalmon", "darkseagreen",
                "darkslateblue", "darkslategray", "darkslategrey",
                "darkturquoise", "darkviolet", "deeppink", "deepskyblue",
                "dimgray", "dimgrey", "dodgerblue", "firebrick",
                "floralwhite", "forestgreen", "fuchsia", "gainsboro",
                "ghostwhite", "gold", "goldenrod", "gray", "grey",
                "greenyellow", "honeydew", "hotpink", "indianred", "indigo",
                "ivory", "khaki", "lavender", "lavenderblush", "lawngreen",
                "lemonchiffon", "lightblue", "lightcoral", "lightcyan",
                "lightgoldenrodyellow", "lightgray", "lightgrey",
                "lightgreen", "lightpink", "lightsalmon", "lightseagreen",
                "lightskyblue", "lightslategray", "lightslategrey",
                "lightsteelblue", "lightyellow", "lime", "limegreen",
                "linen", "magenta", "maroon", "mediumaquamarine",
                "mediumblue", "mediumorchid", "mediumpurple",
                "mediumseagreen", "mediumslateblue", "mediumspringgreen",
                "mediumturquoise", "mediumvioletred", "midnightblue",
                "mintcream", "mistyrose", "moccasin", "navajowhite", "navy",
                "oldlace", "olive", "olivedrab", "orange", "orangered",
                "orchid", "palegoldenrod", "palegreen", "paleturquoise",
                "palevioletred", "papayawhip", "peachpuff", "peru", "pink",
                "plum", "powderblue", "purple", "rosybrown",
                "royalblue", "saddlebrown", "salmon", "sandybrown",
                "seagreen", "seashell", "sienna", "silver", "skyblue",
                "slateblue", "slategray", "slategrey", "springgreen",
                "steelblue", "tan", "teal", "thistle", "tomato", "turquoise",
                "violet", "wheat", "yellow",
                "yellowgreen"
            ] * 5

# matplotlib.pyplot all line styles
linestyle_tuple = [
     ('loosely dotted',        (0, (1, 10))),
     ('dotted',                (0, (1, 1))),
     ('long dash with offset', (5, (10, 3))),
     ('loosely dashed',        (0, (5, 10))),
     ('dashed',                (0, (5, 5))),
     ('densely dashed',        (0, (5, 1))),

     ('loosely dashdotted',    (0, (3, 10, 1, 10))),
     ('dashdotted',            (0, (3, 5, 1, 5))),
     ('densely dashdotted',    (0, (3, 1, 1, 1))),

     ('dashdotdotted',         (0, (3, 5, 1, 5, 1, 5))),
     ('loosely dashdotdotted', (0, (3, 10, 1, 10, 1, 10))),
     ('densely dashdotdotted', (0, (3, 1, 1, 1, 1, 1)))]


def plot_sequentialy(ax, thedata):
    ax.plot((thedata + thedata.max(1).values.cumsum(0).reshape(-1, 1)).T)
    return ax


def better_lookin(ax, fontsize=20, lightmode=False,legend_bbox=None,grid="on",ncol=1,order=None,legend=False):
    for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                 ax.get_xticklabels() + ax.get_yticklabels()):
        item.set_fontsize(fontsize)
    handles, labels = ax.get_legend_handles_labels()
    if order is None:
        order=range(len(handles))
    ax.legend([handles[idx] for idx in order], [labels[idx] for idx in order],fontsize=fontsize,ncol=ncol)
    
    ax.get_legend().set_bbox_to_anchor(bbox=legend_bbox)
    ax.get_legend().set_visible(legend)

    ax.grid(grid)
    if lightmode:
        ax.axis("off")
    else:
        ax.spines['right'].set_visible(False)
        # ax.spines['left'].set_visible(False)
        ax.spines['top'].set_visible(False)

def plot_ci(ax, a,  xx=None, color="black", label=None, lw=None, linestyle=None, alpha=0.3,marker=None):
    """
    Display median +- IQR from a results array. The repetitive results are expected columnwise along axis=1.

    :param ax: axes
    :param a: input (n x m) ndarray,
    :param xx: prefered x-axis of size a.shape[0], if nothing is passed, defaults to np.arange(a.shape[0])
    :param color: passed to pyplot.plot
    :param label: passed to pyplot.plot
    :param linewidth: passed to pyplot.plot
    :param linestyle: passed to pyplot.plot
    :param thelw: Use linewidth instead.
    :param alpha: pyplot.fill_between opacity.
    :return:
    """

    if xx is None:
        the_x = np.arange(a.shape[0])
    else:
        the_x = xx

    ax.plot(the_x, np.nanmedian(a, axis=1), color=color, linewidth=lw, label=label, linestyle=linestyle,marker=marker)
    ax.fill_between(the_x,
                    y1=np.nanpercentile(a, 25, axis=1),
                    y2=np.nanpercentile(a, 75, axis=1), alpha=alpha, color=color, label="_skip")
    return ax


def plot_feat_grid(Xl,Yl,feat_names,h=8,w=3,fontsize=20,perc=0,same=False,lw=6, lbls=None,figsize=(20,40),theplot_opts={"density":True,"bins":50},num_for_nan=None,show=False,save=False,dpi=None):
    """Display empirical marginal distributions in a grid plot."""
    plots_tuple=(h,w)   
    n_classes = Yl[0].shape[1]

    if same:
        Fig = [plt.figure(figsize=figsize)]*n_classes
        Axes = [Fig[0].subplots(h, w)]*n_classes

    else:
        Fig = [plt.figure(figsize=figsize) for _ in range(n_classes)]# plt.figure(figsize=(40,20))]
        Axes = [fig.subplots(h, w) for fig in Fig]
    
    if lbls is None:
        lbls = ["Raw", "NB", "NF"]
    
    colors = ["black", "darkgreen", "darkred"]
    
    #markers = ["o","x","s"]
    markers = [None, None, None]
    linestyles=['solid', 'dashed', 'dashdot', 'dotted']
    
    for iclass,fig in enumerate(Fig):
        print("class", iclass)
        alpha = 1
        themin=[]
        themax=[]
        
        for iset, (X, Y) in enumerate(zip(Xl,Yl)):
            print("set", iset)
            
            if iset > 0:
                alpha = 0.5

            Xclass = X[Y[:, iclass] == 1].copy()
            if iset ==0:
                Xclass=Xclass[(Xclass==num_for_nan).sum(1)==0]
                assert(Xclass.shape[0]>1)
                #Xclass[np.isnan]
            for i in range(Xclass.shape[1]):
                theloc = (i//w, i%w)
                
                ax = Axes[iclass][theloc[0],theloc[1]]
                #plt.subplot2grid(shape=plots_tuple, loc=theloc,fig=fig)
                if iset==0:
                    themin.append(np.percentile(Xclass[:,i],perc))
                    themax.append(np.percentile(Xclass[:,i],100-perc))
                    
                xbin=Xclass[:,i]
                if not ("sex" in feat_names[i]):
                    xbin=xbin[(xbin>=themin[i])&(xbin<=themax[i])]

                weights = np.ones_like(xbin)/len(xbin)
                
                they,thex=np.histogram(xbin,
                                       #weights=weights, 
                                       **theplot_opts)
                
                ax.plot(thex[:-1],they,label=lbls[iset] + "class=" + str(iclass),
                        marker=markers[iset],color=colors[iset],linestyle=linestyles[iclass],
                        linewidth=lw)
                #print(out)
                #sys.exit(1)
                thetitle=feat_names[i].replace("feats__", "")
                
                if thetitle == "cirk_vikt":
                    thetitle="body weight"
                elif thetitle == "bw":
                    thetitle="birth weight"
                elif thetitle == "pnage_days":
                    thetitle="postnatal age"

                thetitle=thetitle.replace("_"," ").replace("btb","IBI").replace("rf","RF").replace("spo2","SpO2")
                ax.set_title(thetitle, loc='left', y=0.97, x=0.02,fontsize=int(fontsize*2))
                # ax.set_xticklabels(ax.get_xticklabels(), rotation=30)

                better_lookin(ax,fontsize=fontsize)
                if i>0:
                    ax.get_legend().remove()
                ax.grid(False)
    if save:
        for iclass,fig in enumerate(Fig):
            fig.tight_layout()
            fig.savefig("plot_feat_grid_Class_{}.pdf".format(iclass),dpi=dpi)
    if show:
        plt.show()
    else:
        [plt.close(fig) for fig in Fig]
        
    return Fig
