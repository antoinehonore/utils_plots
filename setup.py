from setuptools import setup

# from subprocess import check_output
# from setuptools import setup, find_packages
# cmdgit = "git rev-parse --short HEAD";
# cmddate = "date +%Y%m%d";
# version = "v" + check_output(cmddate.split()).decode().strip()+"."+check_output(cmdgit.split()).decode().strip()

setup(
    # Application name:
    name="utils_plots",

    # Version number (initial):
    version="0.1.0",

    # Application author details:
    author="Antoine Honore",
    author_email="ahonore@pm.me",

    # Packages
    packages=["utils_plots"],

    # Include additional files into the package
    include_package_data=True,

    # Details
    url="http://pypi.python.org/pypi/MyApplication_v010/",

    license="GNU GENERAL PUBLIC LICENSE Version 3",
    license_files = ('LICENSE.txt',),
    description="Useful towel-related stuff.",

    # long_description=open("README.txt").read(),

    # Dependent packages (distributions)
    install_requires=["numpy"],
)
